To test the lock-free version of the code:

    cmake -DLOCKFREE=1 <dir>

To test the mutex-based version of the code:

    cmake <dir>
