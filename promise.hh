// (c) jk 2018
/* 
    This program is free software: you can redistribute it and/or modify
    it under the terms of either:
    1) the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version, or
    2) the Apache 2.0 License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You should have received a copy of the Apache 2.0 License along with this program.
    If not, see <https://www.apache.org/licenses/LICENSE-2.0.txt>.
*/    
#ifndef promise_hh_a255e92e_bcee_11e8_8a05_002618833159
#define promise_hh_a255e92e_bcee_11e8_8a05_002618833159

#ifndef PROMISE_IS_LOCKFREE
#define PROMISE_IS_LOCKFREE 1
#endif

#ifndef PROMISE_NAMESPACE
#define PROMISE_NAMESPACE appatis
#endif

#if defined __x86_64__
#include <immintrin.h>
#endif

#include <cassert>
#include <atomic>
#include <exception>
#include <utility>
#include <memory>
#include <vector>
#include <functional>

#include <mutex>
#include <condition_variable>
#include <thread>

#ifndef PROMISE_MUTEX_TYPE
#define PROMISE_MUTEX_TYPE std::mutex
#endif

#if __cplusplus >= 201703L
#include <optional>
namespace PROMISE_NAMESPACE {
template<typename T> using optional_type = std::optional<T>;
}
#elif (__GNUC__ == 6 && __GNUC_MINOR__ >= 3 || __GNUC__ > 6) && __cplusplus >= 201402L
#include <experimental/optional>
namespace PROMISE_NAMESPACE {
template<typename T> using optional_type = std::experimental::optional<T>;
}
#else
#include <boost/optional.hpp>
namespace PROMISE_NAMESPACE {
template<typename T> using optional_type = boost::optional<T>;
}
#endif

namespace PROMISE_NAMESPACE {

template<typename T> class Promise;

namespace internal {

template<typename T> struct PromiseTraits {
    typedef typename std::decay<T>::type value_type;
    typedef Promise<value_type> promise_type;

    template<typename F, typename A>
    static void attach_promise(const promise_type& p, F&& f, A&& a); 
    /*
     * p.resolve(f(a));
     */

    template<typename F>
    static void attach_promise_0(const promise_type& p, F&& f);
    /*
     * p.resolve(f());
     */

};

template<> struct PromiseTraits<void> {
    typedef int value_type;
    typedef Promise<void> promise_type;

    template<typename F, typename A>
    static void attach_promise(const promise_type& p, F&& f, A&& a); 
    /*
     * f(a);
     * p.resolve();
     */

    template<typename F>
    static void attach_promise_0(const promise_type& p, F&& f); 
    /*
     * f();
     * p.resolve();
     */

};

template<typename T> struct PromiseTraits<Promise<T> > {
    typedef typename PromiseTraits<T>::promise_type promise_type;

    template<typename F, typename A>
    static void attach_promise(const promise_type& p, F&& f, A&& a);

    template<typename F>
    static void attach_promise_0(const promise_type& p, F&& f);
};


struct PromiseDetailsBase {

    static const unsigned kThen{1U};
    static const unsigned kOtherwise{2U};
    static const unsigned kBusy{4U};
    static const unsigned kBlocked{8U};
    static const unsigned kDone{kThen | kOtherwise | kBlocked};

    struct Callback {
        virtual ~Callback() = default;
        virtual void then() noexcept {}
        virtual void otherwise(const std::exception_ptr& ep) noexcept = 0;
        Callback *_next{nullptr};
    };

    typedef void (*unhandled_hook_function)(std::exception_ptr);

    static unhandled_hook_function& unhandled() {
        static unhandled_hook_function uf{nullptr};
        return uf;
    }

};

struct PromiseImplBase : PromiseDetailsBase {

    std::exception_ptr _eptr;

#if PROMISE_IS_LOCKFREE
    std::atomic<Callback *> _callback{nullptr};
    std::atomic<unsigned> _state{0U};
    std::atomic<bool> _exception_checked{false};

    unsigned get_state_protected() const { return _state.load(std::memory_order_relaxed); }
#else
    Callback *_callback{nullptr};
    unsigned _state{0U};
    bool _exception_checked{false};
    mutable PROMISE_MUTEX_TYPE _mx;

    unsigned get_state_protected() const { 
        std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
        return _state;
    }
#endif

    PromiseImplBase() = default;
    PromiseImplBase(const PromiseImplBase&) = delete;
    PromiseImplBase& operator=(const PromiseImplBase&) = delete;
    ~PromiseImplBase();

    void block() noexcept;

    void otherwise_continue() noexcept {
#if PROMISE_IS_LOCKFREE
        // check first to avoid bus locking when it is not needed
        if ( !_exception_checked.load(std::memory_order_relaxed) ) {
            _exception_checked.exchange(true); // is plain store enough? not sure
        }
#else
        std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
        _exception_checked = true;
#endif
    }

    void finalize_otherwise(Callback *) noexcept;
    void finalize_then(Callback *) noexcept;
    void finalize_block(Callback *) noexcept;

    // returns true if this Promise object not resolved or rejected before,
    // otherwise does nothing and returns false
    bool reject(std::exception_ptr ep) noexcept {
#if PROMISE_IS_LOCKFREE
        auto state = 0U;
        if ( _state.load(std::memory_order_relaxed) == 0U && _state.compare_exchange_strong(state, kBusy) ) {
            _eptr = std::move(ep);
            state = kBusy;
            if ( _state.compare_exchange_strong(state, kOtherwise) ) {
                // call all the handlers for `otherwise`
                finalize_otherwise(_callback.exchange(nullptr));
                return true;
            }
            else {
                // another thread blocked this Promise instance just after this thread set it to kBusy,
                // so do nothing
                assert ( state == (kBusy | kBlocked) );
            }
        }
        return false;
#else
        Callback *f;
        {
            std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
            if ( _state != 0U ) {
                return false;
            }
            _eptr = std::move(ep);
            _state = kOtherwise;
            f = _callback;
            _callback = nullptr;
        }
        finalize_otherwise(f);
        return true;
#endif
    }

    void install_callback(Callback *bsp) noexcept;
    void destroy_callback(Callback *bsp) noexcept { delete bsp; }

    bool reject_current() noexcept { return reject(std::current_exception()); }

    auto eptr() const { return _eptr; }

};

template<typename T>
    struct PromiseImpl final : public PromiseImplBase {

        typedef typename PromiseTraits<T>::value_type value_type;
        typedef PromiseTraits<T> traits_type;

        optional_type<value_type> _value;

        // returns true if this Promise object not resolved or rejected before,
        // otherwise does nothing and returns false
        template<typename... V> bool resolve(V&&... v) {
#if PROMISE_IS_LOCKFREE
            auto state = 0U;
            if (  _state.load(std::memory_order_relaxed) == 0U &&  _state.compare_exchange_strong(state, kBusy) ) {
                // constructing T in-place
                _value.emplace(std::forward<V>(v)...);    
                state = kBusy;
                if ( _state.compare_exchange_strong(state, kThen) ) {
                    // call all the handlers for `then`
                    finalize_then(_callback.exchange(nullptr));
                    return true;
                }
                else {
                    // another thread blocked this Promise instance just after this thread set it to kBusy,
                    // so do nothing
                    assert ( state == (kBusy | kBlocked) );
                }
            }
            return false;
#else
            Callback *f;
            {
                std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
                if ( _state != 0U ) {
                    return false;
                }
                _value.emplace(std::forward<V>(v)...);    
                _state = kThen;
                f = _callback;
                _callback = nullptr;
            }
            finalize_then(f);
            return true;
#endif
        }

        const auto& value() const { return *_value; }

    };

template<>
    struct PromiseImpl<void> final : public PromiseImplBase {

        bool resolve() {
#if PROMISE_IS_LOCKFREE
            auto state = 0U;
            if ( _state.load(std::memory_order_relaxed) == 0U && _state.compare_exchange_strong(state, kThen) ) {
                finalize_then(_callback.exchange(nullptr));
                return true;
            }
            return false;
#else
            Callback *f;
            {
                std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
                if ( _state != 0U ) {
                    return false;
                }
                _state = kThen;
                f = _callback;
                _callback = nullptr;
            }
            finalize_then(f);
            return true;
#endif
        }

        auto value() const { return 0; }

    };

} // namespace internal

template<typename T> class Promise final : internal::PromiseDetailsBase {

public:
    typedef internal::PromiseTraits<T> traits_type;
    typedef typename traits_type::value_type value_type;
    typedef internal::PromiseImpl<T> impl_type;

private:
    std::shared_ptr<impl_type> _impl;

public:

    Promise() : _impl(std::make_shared<impl_type>()) {
    }

    template<class F> auto then(F&& f) const;
    template<class EXECUTOR, class F> auto then(EXECUTOR&& ex, F&& f) const;

    template<class F> auto otherwise(F&& f) const;
    template<class EXECUTOR, class F> auto otherwise(EXECUTOR&&, F&&) const;

    //
    // The functor parameter for `anyway` method must be able to be called 
    // either with promise value (or void for Promise<void>) or with std::exception_ptr as parameter.
    //
    template<class F> auto anyway(F&& f) const;
    template<class EXECUTOR, class F> auto anyway(EXECUTOR&& ex, F&& f) const;

    bool reject(std::exception_ptr ep) const noexcept {
        return _impl->reject(std::move(ep));
    }

    bool reject_current() const noexcept {
        return _impl->reject_current();
    }

    template<typename EXCEPTION>
        bool reject_with(EXCEPTION&& e) const noexcept {
            return reject(std::make_exception_ptr(std::forward<EXCEPTION>(e)));
        }

    void block() const noexcept {
        _impl->block();
    }

    [[deprecated]] void ignore() const noexcept {
        _impl->otherwise_continue();
    }

    void otherwise_continue() const noexcept {
        _impl->otherwise_continue();
    }

    bool succeeded() const noexcept {
        return _impl->get_state_protected() & kThen;
    }

    bool failed() const noexcept {
        return _impl->get_state_protected() & kOtherwise;
    }

    bool blocked() const noexcept {
        return _impl->get_state_protected() & kBlocked;
    }

    bool done() const noexcept {
        return _impl->get_state_protected() & (kThen | kOtherwise);
    }

    bool done_or_blocked() const noexcept {
        return _impl->get_state_protected() & (kThen | kOtherwise | kBlocked);
    }

    const value_type& get_value() const {
        assert(done());
        if ( _impl->_eptr ) {
            std::rethrow_exception(_impl->_eptr);
        }
        return _impl->value();
    }

    template<typename... P>
        bool resolve(P&&... v) const {
            return _impl->resolve(std::forward<P>(v)...);
        }

private:

    template<typename CALLBACK, typename... ARGS>
    auto install_callback(ARGS&&... args) const {
        auto callback = new CALLBACK(std::forward<ARGS>(args)...);
        auto p = callback->_p;
        _impl->install_callback(callback);
        return p;
    }

    void destroy_callback(Callback *cb) const {
        _impl->destroy_callback(cb);
    }

};



template<typename T> class Promise<Promise<T> > {
private:    
    Promise() = default;
    ~Promise() = delete; // Promises of Promise are forbidden.
};


namespace internal {

template<typename T>
template<typename F, typename A>
void PromiseTraits<T>::attach_promise(const PromiseTraits<T>::promise_type& p, F&& f, A&& a) {
    p.resolve(f(std::forward<A>(a)));
}

template<typename T>
template<typename F>
void PromiseTraits<T>::attach_promise_0(const PromiseTraits<T>::promise_type& p, F&& f) {
    p.resolve(f());
}

template<typename F, typename A>
void PromiseTraits<void>::attach_promise(const PromiseTraits<void>::promise_type& p, F&& f, A&& a) {
    f(std::forward<A>(a));
    p.resolve();
}

template<typename F>
void PromiseTraits<void>::attach_promise_0(const PromiseTraits<void>::promise_type& p, F&& f) {
    f();
    p.resolve();
}

} // namespace internal


namespace internal {

template<typename T> struct PromiseResolver {
    Promise<T> _p;
    PromiseResolver(const Promise<T>& p) : _p(p) {} 

    void operator()(const typename PromiseTraits<T>::value_type& t) const noexcept {
        _p.resolve(t);
    }

    void operator()() const noexcept {
        _p.resolve();
    }

    void operator()(const std::exception_ptr& ep) const noexcept {
        _p.reject(ep);
    }
};

template<typename T> 
PromiseResolver<T> make_promise_resolver(const Promise<T>& p) noexcept {
    return PromiseResolver<T>(p);
}


template<typename R>
template<typename F, typename A>
void PromiseTraits<Promise<R> >::attach_promise(const PromiseTraits<Promise<R>>::promise_type& p, 
                                                                F&& f, 
                                                                A&& a) {
    f(std::forward<A>(a))
        .anyway(internal::make_promise_resolver(p));
}

template<typename R>
template<typename F>
void PromiseTraits<Promise<R> >::attach_promise_0(const PromiseTraits<Promise<R>>::promise_type& p, F&& f) {
    f().anyway(internal::make_promise_resolver(p));
}


template<typename T> struct ParameterTraits {
    template<typename PROMISE, typename F, typename A>
    static void bind_promise(PROMISE p, F&& f, const A& a) noexcept {
        typedef PromiseTraits<decltype(f(a))> traits_type;
        try {
            traits_type::attach_promise(p, std::forward<F>(f), std::cref(a));
        }
        catch ( ... ) {
            p.reject_current();
        }
    }
};


template<> struct ParameterTraits<std::true_type> {
    template<typename PROMISE, typename F, typename A>
    static void bind_promise(PROMISE p, F&& f, A a) noexcept {
        typedef PromiseTraits<decltype(f(a))> traits_type;
        try {
            traits_type::attach_promise(p, std::forward<F>(f), std::move(a));
        }
        catch ( ... ) {
            p.reject_current();
        }
    }
};


template<typename T> using BestParameterTraits =
    ParameterTraits<std::integral_constant<bool, std::is_trivially_copy_constructible<T>::value && sizeof(T) <= 2 * sizeof(long)>>;


template<typename PROMISE, typename F>
void bind_promise(PROMISE p, F&& f) noexcept {
    typedef PromiseTraits<decltype(f())> traits_type;
    try {
        traits_type::attach_promise_0(p, std::forward<F>(f));
    }
    catch ( ... ) {
        p.reject_current();
    }
}



    template<typename A, typename F>
    struct ThenCallback final : public PromiseDetailsBase::Callback {

#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(A)>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, A>::type>::type result_type;
#endif

        F _f;
        typename PromiseTraits<result_type>::promise_type _p;
        optional_type<A> *_pvalue;

        template<typename G>
        ThenCallback(G&& f, PromiseImpl<A> *impl) : 
            _f(std::forward<G>(f)), _pvalue(&impl->_value) {}

        virtual void then() noexcept override final {
            BestParameterTraits<A>::bind_promise(_p, std::move(_f), _pvalue->value());
        }
        virtual void otherwise(const std::exception_ptr& ep) noexcept override final {
            _p.reject(ep);
        }
    };

    template<typename F>
    struct ThenCallback<void, F> final : public PromiseDetailsBase::Callback {

#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F()>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F>::type>::type result_type;
#endif

        F _f;
        typename PromiseTraits<result_type>::promise_type _p;

        template<typename G>
        ThenCallback(G&& f, PromiseImpl<void> *) : 
            _f(std::forward<G>(f)) {}

        virtual void then() noexcept override final {
            bind_promise(_p, std::move(_f));
        }
        virtual void otherwise(const std::exception_ptr& ep) noexcept override final {
            _p.reject(ep);
        }
    };

    template<typename A, typename F, typename EXECUTOR>
    struct ThenXCallback final : public PromiseDetailsBase::Callback {

#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(A)>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, A>::type>::type result_type;
#endif

        F _f;
        EXECUTOR ex_;
        typename PromiseTraits<result_type>::promise_type _p;
        optional_type<A> *_pvalue;

        template<typename G, typename EX>
        ThenXCallback(G&& f, EX&& ex, PromiseImpl<A> *impl) : 
            _f(std::forward<G>(f)), ex_{std::forward<EX>(ex)}, _pvalue(&impl->_value) {}

        virtual void then() noexcept override final try {
            ex_([f = std::move(_f), p = _p, value = _pvalue->value()] {
                ParameterTraits<std::true_type>::bind_promise(std::move(p), std::move(f), std::move(value));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final try {
            ex_([p = _p, ep] {
                p.reject(std::move(ep));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }
    };

    template<typename F, typename EXECUTOR>
    struct ThenXCallback<void, F, EXECUTOR> final : public PromiseDetailsBase::Callback {

#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F()>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F>::type>::type result_type;
#endif

        F _f;
        EXECUTOR ex_;
        typename PromiseTraits<result_type>::promise_type _p;

        template<typename G, typename EX>
        ThenXCallback(G&& f, EX&& ex, PromiseImpl<void> *) : 
            _f(std::forward<G>(f)), ex_{std::forward<EX>(ex)} {}

        virtual void then() noexcept override final try {
            ex_([f = std::move(_f), p = _p] {
                bind_promise(p, std::move(f));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final try {
            ex_([p = _p, ep1 = std::move(ep)] {
                p.reject(ep1);
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }
    };

    template<typename A, typename F>
    struct OtherwiseCallback final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result_type;
#endif

        F _f;
        typename PromiseTraits<result_type>::promise_type _p;

        template<typename G>
        OtherwiseCallback(G&& g) : _f(std::forward<G>(g)) {}

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final {
            BestParameterTraits<std::exception_ptr>::bind_promise(_p, std::move(_f), ep);
        }
    };

    template<typename A, typename F, typename EXECUTOR>
    struct OtherwiseXCallback final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result_type;
#endif

        F _f;
        EXECUTOR ex_;
        typename PromiseTraits<result_type>::promise_type _p;

        template<typename G, typename EX>
        OtherwiseXCallback(G&& g, EX&& ex) : _f(std::forward<G>(g)), ex_{std::forward<EX>(ex)} {}

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final try {
            ex_([f = std::move(_f), ep1 = ep, p = _p] {
                ParameterTraits<std::true_type>::bind_promise(p, std::move(f), std::move(ep1));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }
    };

    template<typename A, typename F>
    struct AnywayCallback final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result1_type;
        typedef typename std::decay<typename std::result_of<F(A)>::type>::type result0_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result1_type;
        typedef typename std::decay<typename std::invoke_result<F, A>::type>::type result0_type;
#endif

        static_assert(std::is_same<typename PromiseTraits<result1_type>::promise_type,
                                   typename PromiseTraits<result0_type>::promise_type>::value,
                      "Result types for `then` and `otherwise` should be the same");

        F _f;
        optional_type<A> *_pvalue;
        typename PromiseTraits<result0_type>::promise_type _p;

        template<typename G>
        AnywayCallback(G&& g, PromiseImpl<A> *impl) : 
            _f(std::forward<G>(g)),
            _pvalue(&impl->_value) {}

        virtual void then() noexcept override final {
            BestParameterTraits<A>::bind_promise(_p, std::move(_f), _pvalue->value());
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final {
            BestParameterTraits<std::exception_ptr>::bind_promise(_p, std::move(_f), ep);
        }
    };

    template<typename F>
    struct AnywayCallback<void, F> final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result1_type;
        typedef typename std::decay<typename std::result_of<F()>::type>::type result0_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result1_type;
        typedef typename std::decay<typename std::invoke_result<F>::type>::type result0_type;
#endif

        static_assert(std::is_same<typename PromiseTraits<result1_type>::promise_type,
                                   typename PromiseTraits<result0_type>::promise_type>::value,
                      "Result types for `then` and `otherwise` should be the same");

        F _f;
        typename PromiseTraits<result0_type>::promise_type _p;

        template<typename G>
        AnywayCallback(G&& g, PromiseImpl<void> *) : 
            _f(std::forward<G>(g)) {}

        virtual void then() noexcept override final {
            bind_promise(_p, std::move(_f));
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final {
            BestParameterTraits<std::exception_ptr>::bind_promise(_p, std::move(_f), ep);
        }
    };


    template<typename A, typename F, typename EXECUTOR>
    struct AnywayXCallback final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result1_type;
        typedef typename std::decay<typename std::result_of<F(A)>::type>::type result0_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result1_type;
        typedef typename std::decay<typename std::invoke_result<F, A>::type>::type result0_type;
#endif
        static_assert(std::is_same<typename PromiseTraits<result1_type>::promise_type,
                                   typename PromiseTraits<result0_type>::promise_type>::value,
                      "Result types for `then` and `otherwise` should be the same");

        F _f;
        optional_type<A> *_pvalue;
        EXECUTOR ex_;
        typename PromiseTraits<result0_type>::promise_type _p;

        template<typename G, typename EX>
        AnywayXCallback(G&& g, EX&& ex, PromiseImpl<A> *impl) : 
            _f(std::forward<G>(g)),
            ex_{std::forward<EX>(ex)},
            _pvalue(&impl->_value) {}

        virtual void then() noexcept override final try {
            ex_([f = std::move(_f), p = _p, value = _pvalue->value()] {
                ParameterTraits<std::true_type>::bind_promise(p, std::move(f), std::move(value));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final try {
            ex_([f = std::move(_f), p = _p, ep] {
                ParameterTraits<std::true_type>::bind_promise(p, std::move(f), std::move(ep));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }
    };

    template<typename F, typename EXECUTOR>
    struct AnywayXCallback<void, F, EXECUTOR> final : public PromiseDetailsBase::Callback {
#if __cplusplus < 201700L
        typedef typename std::decay<typename std::result_of<F(std::exception_ptr)>::type>::type result1_type;
        typedef typename std::decay<typename std::result_of<F()>::type>::type result0_type;
#else
        typedef typename std::decay<typename std::invoke_result<F, std::exception_ptr>::type>::type result1_type;
        typedef typename std::decay<typename std::invoke_result<F>::type>::type result0_type;
#endif

        static_assert(std::is_same<typename PromiseTraits<result1_type>::promise_type,
                                   typename PromiseTraits<result0_type>::promise_type>::value,
                      "Result types for `then` and `otherwise` should be the same");

        F _f;
        EXECUTOR ex_;
        typename PromiseTraits<result0_type>::promise_type _p;

        template<typename G, typename EX>
        AnywayXCallback(G&& g, EX&& ex, PromiseImpl<void> *) : 
            _f(std::forward<G>(g)),
            ex_{std::forward<EX>(ex)}
        {}

        virtual void then() noexcept override final try {
            ex_([f = std::move(_f), p = _p] {
                bind_promise(p, std::move(f));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }

        virtual void otherwise(const std::exception_ptr& ep) noexcept override final try {
            ex_([f = std::move(_f), p = _p, ep] {
                ParameterTraits<std::true_type>::bind_promise(p, std::move(f), std::move(ep));
            });
        }
        catch ( ... ) {
            _p.reject_current();
        }
    };

}



template<class R>
template<class F> auto Promise<R>::then(F&& f) const 
{
#if __cplusplus >= 201700L
    static_assert( (std::is_same_v<R, void> && std::is_invocable<F>::value) || std::is_invocable<F, R>::value,
                  "Wrong argument type for the handler");
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;

    return install_callback<internal::ThenCallback<R, functor_type>>
        (std::forward<F>(f), _impl.get());
}



template<class R>
template<class EXECUTOR, class F> auto Promise<R>::then(EXECUTOR&& ex, F&& f) const 
{
#if __cplusplus >= 201700L
    static_assert( (std::is_same_v<R, void> && std::is_invocable<F>::value) || std::is_invocable<F, R>::value,
                  "Wrong argument type for the handler");
    static_assert( std::is_invocable<EXECUTOR, void()>::value, "Wrong executor signature" );
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;

    return install_callback<internal::ThenXCallback<R, functor_type, EXECUTOR>>
        (std::forward<F>(f), std::forward<EXECUTOR>(ex), _impl.get());
}



template<class R>
template<class F> auto Promise<R>::otherwise(F&& f) const
{
#if __cplusplus >= 201700L
    static_assert(std::is_invocable<F, std::exception_ptr>::value,
                  "Wrong argument type for the handler");
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;

    return install_callback<internal::OtherwiseCallback<R, functor_type>>(std::forward<F>(f));
}


template<class R>
template<class EXECUTOR, class F> auto Promise<R>::otherwise(EXECUTOR&& ex, F&& f) const
{
#if __cplusplus >= 201700L
    static_assert(std::is_invocable<F, std::exception_ptr>::value,
                  "Wrong argument type for the handler");
    static_assert( std::is_invocable<EXECUTOR, void()>::value, "Wrong executor signature" );
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;

    return install_callback<internal::OtherwiseXCallback<R, functor_type, EXECUTOR>>
        (std::forward<F>(f), std::forward<EXECUTOR>(ex));
}


template<typename R>
template<typename F> auto Promise<R>::anyway(F&& f) const {
#if __cplusplus >= 201700L
    static_assert( (std::is_same_v<R, void> && std::is_invocable<F>::value) || std::is_invocable<F, R>::value,
                  "Wrong argument type for the handler");
    static_assert(std::is_invocable<F, std::exception_ptr>::value,
                  "Wrong argument type for the handler");
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;
    return install_callback<internal::AnywayCallback<R, functor_type>>
        (std::forward<F>(f), _impl.get());
}


template<typename R>
template<class EXECUTOR, typename F> auto Promise<R>::anyway(EXECUTOR&& ex, F&& f) const {
#if __cplusplus >= 201700L
    static_assert( (std::is_same_v<R, void> && std::is_invocable<F>::value) || std::is_invocable<F, R>::value,
                  "Wrong argument type for the handler");
    static_assert(std::is_invocable<F, std::exception_ptr>::value,
                  "Wrong argument type for the handler");
    static_assert( std::is_invocable<EXECUTOR, void()>::value, "Wrong executor signature" );
#endif
    typedef typename std::decay<decltype(f)>::type functor_type;
    return install_callback<internal::AnywayXCallback<R, functor_type, EXECUTOR>>
        (std::forward<F>(f), std::forward<EXECUTOR>(ex), _impl.get());
}

// all

namespace internal {

template<typename TUPLE, typename PROMISE, typename ARG> 
    void promise_chain_all(TUPLE&& tpl, PROMISE p, Promise<ARG> a1)
{
    a1.then([tpl1 = std::forward<TUPLE>(tpl), p](const ARG& result) {
        p.resolve(std::tuple_cat(tpl1, std::tuple<ARG>(result)));
    })
    .otherwise(make_promise_resolver(p));
}


template<typename TUPLE, typename PROMISE, typename ARG, typename... ARGS> 
    void promise_chain_all(TUPLE&& tpl, PROMISE p, Promise<ARG> a1, Promise<ARGS>... a)
{
    a1.then([tpl1 = std::forward<TUPLE>(tpl), p, a...](const ARG& result) {
        promise_chain_all(std::tuple_cat(tpl1, std::tuple<ARG>(result)),
              p,
              a...);
    })
    .otherwise(make_promise_resolver(p));
}

} // namespace internal


template<typename ARG, typename... ARGS> auto all(Promise<ARG> arg, Promise<ARGS>... args) 
    -> Promise<std::tuple<ARG, ARGS...>>
{
    Promise<std::tuple<ARG, ARGS...>> result_promise;
    internal::promise_chain_all(std::tuple<>(), result_promise, arg, args...);
    return result_promise;
}


namespace internal {
    template<typename ARG> void emplace(const std::vector<Promise<ARG>> args, Promise<std::vector<ARG>>& promise, std::vector<ARG>& result, size_t index) {
        if ( index < args.size() ) {
            args[index].then([&args, &promise, &result, index](const ARG& a) {
                result.push_back(a);
                emplace(args, promise, result, index + 1);
            })
            .otherwise([&promise](std::exception_ptr ep) {
                promise.reject(ep);
            });
        }
        else {
            promise.resolve(result);
        }
    }
}

template<typename ARG> Promise<std::vector<ARG>> all(const std::vector<Promise<ARG>>& args)
{
    std::vector<ARG> result_vector;
    Promise<std::vector<ARG>> result_promise;
    internal::emplace(args, result_promise, result_vector, 0);
}



// any
namespace internal {

    template<int INDEX, typename TUPLE_TYPE, typename PROMISE_TYPE, typename ARG>
    void attach_promises(TUPLE_TYPE& tpl, PROMISE_TYPE promise, Promise<ARG> pa) {
        typedef typename std::remove_reference<decltype(tpl)>::type tuple_type;
        pa.then([tpl, promise](const ARG& a) {
            try {
                tuple_type new_tpl(tpl);
                std::get<INDEX>(new_tpl).emplace(a);
                promise.resolve(std::tuple<int, tuple_type>(INDEX, std::move(new_tpl)));
            }
            catch ( ... ) {
                promise.reject_current();
            }
        });
    }

    template<int INDEX, typename TUPLE_TYPE, typename PROMISE_TYPE, typename ARG, typename... ARGS>
    void attach_promises(TUPLE_TYPE& tpl, PROMISE_TYPE promise, Promise<ARG> pa,
                         Promise<ARGS>... args) {
        attach_promises<INDEX>(tpl, promise, pa);
        attach_promises<INDEX + 1>(tpl, promise, args...);
    }

}


template<typename... ARGS> auto any(Promise<ARGS>... args)
    -> Promise< std::tuple<int, std::tuple<optional_type<ARGS>...>> >
{
    typedef std::tuple<optional_type<ARGS>...> tuple_type;
    Promise< std::tuple<int, tuple_type> > result_promise;
    tuple_type tt = std::forward_as_tuple(optional_type<typename decltype(args)::value_type>()...);
    internal::attach_promises<0>(tt, result_promise, args...);
    return result_promise;
}


template<typename ARG> Promise<std::tuple<size_t, ARG>> any(const std::vector<Promise<ARG>>& args) {
    Promise<std::tuple<size_t, ARG>> promise;
    for ( size_t i = 0; i < args.size(); ++i ) {
        args[i].then([promise, i](const ARG& v) { promise.resolve(std::make_tuple(i, v)); })
            .otherwise([promise](std::exception_ptr ep) { promise.reject(ep); });
    }
    return promise;
}


namespace internal {
    template<typename F>
    class PromiseFunctionWrapper {
        F f_;

    public:
        typedef typename PromiseTraits< decltype(f_())>::promise_type promise_type;

    private:
        promise_type promise_;

    public:
        template<typename G> PromiseFunctionWrapper(G&& g) : f_(std::forward<G>(g)) {}

        void operator()() const {
            bind_promise(promise_, f_);
        }

        auto get_promise() const { return promise_; }
    };

}



template<typename EXECUTOR, typename F>
auto post(EXECUTOR&& ex, F&& f) {
    internal::PromiseFunctionWrapper<typename std::decay<F>::type> task(std::forward<F>(f));
    auto promise = task.get_promise();
    std::forward<EXECUTOR>(ex)(std::move(task));
    return promise;
}

// wait_for_value
// waiting for promise value _in this very thread_, like std::future::wait_for_value().
//
// This function is the only place where mutex is needed when compiled with PROMISE_IS_LOCKFREE=1.
//
//
namespace internal {
    struct Nop {
        std::mutex *mx_;
        std::condition_variable *cond_;
        bool *notified_;

        void notify() {
            std::unique_lock<std::mutex> l(*mx_);
            *notified_ = true;
            cond_->notify_all();
        }

        template<typename Q> void operator()(Q&&) {
            notify();
        }

        void operator()() {
            notify();
        }
    };
} // namespace internal


template<typename T>
const typename internal::PromiseTraits<T>::value_type& wait_for_value(const Promise<T>& promise) {
    if ( !promise.done() ) {
        std::mutex mx;
        std::condition_variable cond;
        bool notified{false};

        promise.anyway(std::move(internal::Nop{&mx, &cond, &notified}));
        
        std::unique_lock<std::mutex> lock(mx);
        while ( !notified ) {
            cond.wait(lock);
        }
    }

    return promise.get_value();
}


namespace internal {
 
inline void PromiseImplBase::finalize_block(Callback *f) noexcept {
    while ( f ) {
        auto n = f->_next;
        destroy_callback(f);
        f = n;
    }
}


inline void PromiseImplBase::finalize_otherwise(Callback *f) noexcept {
    while ( f ) {
        auto n = f->_next;
        f->otherwise(_eptr);
        destroy_callback(f);
        f = n;
    }
}

inline void PromiseImplBase::finalize_then(Callback *f) noexcept {
    while ( f ) {
        auto n = f->_next;
        f->then();
        destroy_callback(f);
        f = n;
    }
}

inline void PromiseImplBase::block() noexcept {
#if PROMISE_IS_LOCKFREE
    _exception_checked.store(true, std::memory_order_relaxed);
    auto state = _state.load(std::memory_order_relaxed);

    while ( !_state.compare_exchange_weak(state, state | kBlocked) ) {
#if defined __x86_64__
        _mm_pause();
#endif
    }

    auto f = _callback.exchange(nullptr);
#else
    _mx.lock();
    _exception_checked = true;
    _state |= kBlocked;
    Callback *f = _callback;
    _callback = nullptr;
    _mx.unlock();
#endif
    finalize_block(f);
}


inline void PromiseImplBase::install_callback(Callback *bsp) noexcept {
#if PROMISE_IS_LOCKFREE
    _exception_checked.store(true, std::memory_order_relaxed);
    bsp->_next = _callback.load(std::memory_order_relaxed);
    while ( !_callback.compare_exchange_weak(bsp->_next, bsp) ) {
#if defined __x86_64__
        _mm_pause();
#endif
    }

    auto state = _state.load(std::memory_order_acquire);

    if ( state & kDone ) {
        Callback *list = _callback.exchange(nullptr);
        if ( state & kBlocked ) {
            finalize_block(list);
        }
        else if ( state & kThen ) {
            finalize_then(list);
        }
        else {
            assert ( state & kOtherwise );
            finalize_otherwise(list);
        }
    }
#else
    unsigned state;
    {
        std::lock_guard<PROMISE_MUTEX_TYPE> l(_mx);
        _exception_checked = true;
        if ( ( (state = _state) & kDone ) ) {
            assert(!_callback);
        }
        else {
            bsp->_next = _callback;
            _callback = bsp;
            bsp = nullptr;
        }
    }
    if ( bsp ) {
        if ( !(state & kBlocked) ) {
            if ( state & kThen ) {
                bsp->then();
            }
            else {
                assert ( state & kOtherwise );
                bsp->otherwise(_eptr);
            }
        }
        destroy_callback(bsp);
    }
#endif
}

inline PromiseImplBase::~PromiseImplBase() {
#if PROMISE_IS_LOCKFREE
    auto callback = _callback.exchange(nullptr);
    finalize_block(callback);
    if ( _eptr && !_exception_checked.load(std::memory_order_relaxed) ) {
#else
    finalize_block(_callback);
    if ( _eptr && !_exception_checked ) {
#endif
       if ( auto u = unhandled() ) {
           (*u)(_eptr);
       }
       else {
           std::rethrow_exception(_eptr); // terminate the program
       }
    }
}

} // namespace internal

inline internal::PromiseDetailsBase::unhandled_hook_function set_unhandled_hook(internal::PromiseDetailsBase::unhandled_hook_function f) {
    auto u = internal::PromiseDetailsBase::unhandled();
    internal::PromiseDetailsBase::unhandled() = f;
    return u;
}


} // namespace


#endif // promise_hh_a255e92e_bcee_11e8_8a05_002618833159
