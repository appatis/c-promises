#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "promise.hh"

#include <thread>
#include <tuple>
#include <unistd.h>
#include <string>
#include <vector>
#include <atomic>
#include <chrono>

using appatis::Promise;
using appatis::post;
using std::string;
using std::tuple;
using std::get;


class ThreadGroup {
    std::vector<std::thread> threads_;
    std::atomic<unsigned> stage_{0};
public:
    ~ThreadGroup() {
        join();
    }

    template<typename F>
        auto run_in_thread(F&& f, std::chrono::milliseconds delay) {
            threads_.emplace_back(std::thread([f1 = std::forward<F>(f), delay] {
                std::this_thread::sleep_for(delay);
                f1();
            }));
        }

    template<typename T>
        Promise<T> delay(T&& v, std::chrono::milliseconds delay) {
            Promise<T> p;
            threads_.emplace_back(std::thread([p, v1 = std::forward<T>(v), delay] () {
                std::this_thread::sleep_for(delay);
                p.resolve(v1);
            }));
            return p;
        }

    void join() {
        for ( auto& thread: threads_ ) {
            thread.join();
        }
        threads_.clear();
    }

    bool stage(unsigned expected) {
        return stage_++ == expected;
    }

    bool stage_one_of(const std::initializer_list<unsigned>& expected) {
        auto stage = stage_++;
        for ( const auto& s: expected ) {
            if ( stage == s ) {
                return true;
            }
        }
        return false;
    }
};


struct PromiseTest: public ::testing::Test, public ThreadGroup {
    MOCK_METHOD(void, trace, (int));

    static bool hooked;

    virtual void SetUp() {
        hooked = false;
    }

    virtual void TearDown() {
        appatis::set_unhandled_hook(nullptr);
    }

};


bool PromiseTest::hooked{false};

void test_hook(std::exception_ptr) {
    PromiseTest::hooked = true;
}


struct MockException : public std::exception {
    int value_;
    MockException(int v) : value_(v) {}
    MockException() : value_(rand()) {}

    bool operator==(const MockException& other) const { return value_ == other.value_; }
};



class LargeObject {
    std::vector<int> v_;
    int padding[128];
public:
    LargeObject() {
        for ( unsigned i = 0; i < 128; ++i ) {
            v_.push_back(rand());
            padding[i] = rand();
        }
    }

    bool operator==(const LargeObject& lo) const {
        return v_ == lo.v_ && memcmp(padding, lo.padding, sizeof(padding)) == 0;
    }
};

class LargeObjectNoncopyable : public LargeObject {
public:
    LargeObjectNoncopyable() = default;
    LargeObjectNoncopyable(const LargeObjectNoncopyable&) = delete;
    LargeObjectNoncopyable(LargeObjectNoncopyable&&) = default;


    LargeObject get_data() const {
        return LargeObject(*this);
    }

};




class ThreadExecutor {
    std::thread thread_;
    std::chrono::milliseconds delay_;
public:
    ThreadExecutor(std::chrono::milliseconds delay) : delay_(delay) {}
    ~ThreadExecutor() {
        thread_.join();
    }

    template<typename F>
        auto operator()(F&& f) {
            thread_ = std::thread([g = std::forward<F>(f), delay = delay_] {
                std::this_thread::sleep_for(delay);
                g();
            });
        }

    bool in_thread() const {
        return std::this_thread::get_id() == thread_.get_id();
    }
};



TEST_F(PromiseTest, test_basic_resolve_0_void) {
    EXPECT_CALL(*this, trace(1)).Times(1);
    stage(0);
    Promise<void> p;
    p.then([this] {
        trace(1);
        stage(2);
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    .otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    p.otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    stage(1);
    EXPECT_TRUE(p.resolve()); // only first one of reject/resolve succeeded.
    stage(3);
    trace(2);
    EXPECT_FALSE(p.resolve());
    EXPECT_FALSE(p.reject_with(MockException()));
}


TEST_F(PromiseTest, test_basic_reject_0_void) {
    EXPECT_CALL(*this, trace(1)).Times(2);
    stage(0);

    MockException mex;

    Promise<void> p;

    p.then([this] {
        FAIL() << "Shouldn't get here.";
    })
    .otherwise([this, &mex](const std::exception_ptr& ep) {
        trace(1);
        stage_one_of({2, 3});
        EXPECT_CALL(*this, trace(2)).Times(1);
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_TRUE(me == mex);
        }
    });

    p.otherwise([this, &mex](const std::exception_ptr& ep) {
        trace(1);
        stage_one_of({2, 3});
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_TRUE(me == mex);
        }
    });

    stage(1);

    try {
        throw mex;
    }
    catch(...) {
        EXPECT_TRUE(p.reject_current()); // first call succeeds, others fail
        EXPECT_FALSE(p.reject_with(MockException()));
        EXPECT_FALSE(p.resolve());
    }

    stage(4);
    trace(2);
}


TEST_F(PromiseTest, test_basic_resolve_0_int) {
    int value{rand()};
    Promise<int> p;

    EXPECT_CALL(*this, trace(1)).Times(1);
    stage(0);
    p.then([this, value](int v) {
        trace(1);
        stage(2);
        EXPECT_CALL(*this, trace(2)).Times(1);
        EXPECT_EQ(v, value);
    })
    .otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    p.otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });
    stage(1);
    EXPECT_TRUE(p.resolve(value));
    EXPECT_FALSE(p.resolve(value));
    EXPECT_FALSE(p.reject_with(MockException()));
    stage(3);
    trace(2);
}


TEST_F(PromiseTest, test_basic_reject_0_int) {
    Promise<int> p;

    EXPECT_CALL(*this, trace(1)).Times(2);
    stage(0);

    MockException mex;

    p.then([this](int) {
        FAIL() << "Shouldn't get here.";
    })
    .otherwise([this, &mex](const std::exception_ptr& ep) {
        trace(1);
        stage_one_of({2, 3});
        EXPECT_CALL(*this, trace(2)).Times(1);
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_TRUE(me == mex);
        }
    });

    p.otherwise([this, &mex](const std::exception_ptr& ep) {
        trace(1);
        stage_one_of({2, 3});
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_TRUE(me == mex);
        }
    });

    stage(1);

    try {
        throw mex;
    }
    catch(...) {
        EXPECT_TRUE(p.reject_current());
        EXPECT_FALSE(p.reject_current());
        EXPECT_FALSE(p.resolve(0));
    }

    stage(4);
    trace(2);
}



TEST_F(PromiseTest, test_basic_resolve_0_large) {
    LargeObject srcobj;
    Promise<LargeObject> p;

    EXPECT_CALL(*this, trace(1)).Times(1);
    stage(0);
    p.then([this, &srcobj](const LargeObject& lobj) {
        trace(1);
        stage(2);
        EXPECT_CALL(*this, trace(2)).Times(1);
        EXPECT_EQ(lobj, srcobj);
    })
    .otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    p.otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    stage(1);
    p.resolve(srcobj);
    stage(3);
    trace(2);
}


TEST_F(PromiseTest, test_basic_resolve_0_noncopyable) {
    LargeObjectNoncopyable srcobj;
    Promise<LargeObjectNoncopyable> p;

    auto saved_copy = srcobj.get_data();

    EXPECT_CALL(*this, trace(1)).Times(1);
    stage(0);
    p.then([this, &saved_copy](const LargeObjectNoncopyable& lobj) {
        trace(1);
        stage(2);
        EXPECT_CALL(*this, trace(2)).Times(1);
        EXPECT_EQ(lobj, saved_copy);
    })
    .otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    p.otherwise([this](const std::exception_ptr& ep) {
        FAIL() << "Shouldn't get here.";
    });

    stage(1);
    p.resolve(std::move(srcobj));
    stage(3);
    trace(2);
}





TEST_F(PromiseTest, test0) {
    Promise<int> pi;

    EXPECT_CALL(*this, trace(1)).Times(1);

    pi.then([this](int v) {
        trace(1);
        EXPECT_TRUE(stage(1));
        EXPECT_EQ(v, 42);
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    .otherwise([](std::exception_ptr) {
        FAIL() << "Shouldn't get here, for there was no exception raised before.\n";
    })
    ;

    EXPECT_TRUE(stage(0));
    pi.resolve(42);
    trace(2);
    EXPECT_TRUE(stage(2));
}

                
TEST_F(PromiseTest, test00) {
    Promise<int> pi;

    EXPECT_CALL(*this, trace(1)).Times(1);

    pi.then([](int v) {
        FAIL() << "Shouldn't get here, for there was an exception raised before.\n";
    })
    .otherwise([this](std::exception_ptr ep) {
        trace(1);
        EXPECT_TRUE(stage(1));
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            ADD_FAILURE() << "Shouldn't get here.";
        }
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    ;

    EXPECT_TRUE(stage(0));
    pi.reject_with(MockException(0xdeadbeef));
    trace(2);
    EXPECT_TRUE(stage(2));
}


TEST_F(PromiseTest, test1) {
    EXPECT_TRUE(stage(0));
    EXPECT_CALL(*this, trace(1)).Times(1);
    delay(42, std::chrono::milliseconds(500))
    .then([this](int v) {
        trace(1);
        EXPECT_TRUE(stage(1));
        EXPECT_EQ(v, 42);
    })
    .otherwise([](std::exception_ptr) {
        FAIL() << "Shouldn't get here, for there was no exception raised before.\n";
    })
    ;
    join();
    EXPECT_TRUE(stage(2));

}

TEST_F(PromiseTest, test2) {
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_TRUE(stage(0));

    struct AnywayHandler1 {
        PromiseTest *ptest;
        AnywayHandler1(PromiseTest *pt) : ptest{pt} {}
        void operator()(int value) const {
            EXPECT_EQ(value, 0xdeadbeef);
            ptest->trace(3);
        }
        void operator()(std::exception_ptr ep) {
            FAIL() << "Shouldn't get here.";
        }
    };

    struct AnywayHandler0 {
        void operator()() const {
            FAIL() << "Shouldn't get here.";
        }
        void operator()(std::exception_ptr ep) {
            FAIL() << "Shouldn't get here.";
        }
    };


    delay(42, std::chrono::milliseconds(500))
    .then([this](int v) {
        EXPECT_TRUE(stage(1));
        trace(1);
        EXPECT_EQ(v, 42);
        EXPECT_CALL(*this, trace(2)).Times(1);
        return v + 1;
    })
    .then([this](int v) {
        EXPECT_TRUE(stage(2));
        trace(2);
        EXPECT_EQ(v, 43);
        EXPECT_CALL(*this, trace(3)).Times(1);
        return 0xdeadbeef;
    })
    .anyway(AnywayHandler1(this))
    .otherwise([](std::exception_ptr) {
        FAIL() << "Shouldn't get here, for there was no exception raised before.\n";
    })
    .then([] {
        FAIL() << "Shouln't get here, because the previous handler was not fired.\n";
    })
    .anyway(AnywayHandler0())
    ;
    join();

}

TEST_F(PromiseTest, test3) {
    EXPECT_CALL(*this, trace(1)).Times(1);

    struct AnywayHandler1 {
        PromiseTest *ptest;
        AnywayHandler1(PromiseTest *pt) : ptest{pt} {}
        void operator()() const {
            ptest->trace(5);
        }
        void operator()(std::exception_ptr ep) {
            FAIL() << "Shouldn't get here.";
        }
    };

    struct AnywayHandler0 {
        PromiseTest *ptest;
        AnywayHandler0(PromiseTest *pt) : ptest{pt} {}
        void operator()() const {
            FAIL() << "Shouldn't get here.";
        }
        void operator()(std::exception_ptr ep) {
            ptest->trace(2);
            EXPECT_CALL(*ptest, trace(3)).Times(1);
            std::rethrow_exception(ep);
        }
    };

    delay(42, std::chrono::milliseconds(500))
    .then([this](int v) {
        trace(1);
        EXPECT_EQ(v, 42);
        EXPECT_CALL(*this, trace(2)).Times(1);
        throw std::runtime_error("Some exception");
        return v + 1;
    })
    .then([this](int v) {
        FAIL() << "Shouldn't get here, for there was an exception raised before.\n";
    })
    .anyway(AnywayHandler0(this))
    .then([this] { // will compile only with (void) prototype, for the previous handler has void return type
        FAIL() << "Shouldn't get here, for there was an exception raised before.\n";
    })
    .otherwise([this](std::exception_ptr ep) {
        trace(3);
        EXPECT_CALL(*this, trace(4)).Times(1);
        try {
            std::rethrow_exception(ep);
        }
        catch(std::runtime_error& e) {
            trace(4);
            EXPECT_CALL(*this, trace(5)).Times(1);
            EXPECT_STREQ(e.what(), "Some exception");
        }
        catch(...) {
            FAIL() << "The exception is handled already.";
        }
    })
    .anyway(AnywayHandler1(this))
    ;
    join();
}




TEST_F(PromiseTest, test4) {

    EXPECT_CALL(*this, trace(1)).Times(1);

    auto p1 = delay(111, std::chrono::milliseconds(500));
    auto p2 = delay(std::string("112"), std::chrono::milliseconds(200));

    p1
    .then([this](int v) -> int {
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
        EXPECT_EQ(v, 111);
        return v + 1;
    })
    .then([&,this](int v) {
        trace(2);
        EXPECT_CALL(*this, trace(3)).Times(1);
        EXPECT_EQ(v, 112);
        return p2; // returning a promise
    })
    .then([this](const string& v) { // handler gets the promise result
        trace(3);
        EXPECT_EQ(v, std::string("112"));
    })
    .otherwise([](std::exception_ptr ep) {
        FAIL() << "Shouldn't get here, for there was no exception raised before.\n";
        std::rethrow_exception(ep);
        })
    .then([] {
        FAIL() << "Shouln't get here, because the previous handler was not fired.\n";
    })
    ;
    join();

}

TEST_F(PromiseTest, test5) {
    EXPECT_CALL(*this, trace(1)).Times(3);
    EXPECT_CALL(*this, trace(2)).Times(1);

    all(
        delay(1, std::chrono::milliseconds(1000 + (random() % 1500)))
        .then([this](int v) { 
            trace(1);
            EXPECT_EQ(v, 1);
            return v * 10;
        }),

        delay(2, std::chrono::milliseconds(1000 + (random() % 1500)))
        .then([this](int v) { 
            trace(1);
            EXPECT_EQ(v, 2);
            return v * 10;
        }),

        delay(std::string("hello"), std::chrono::milliseconds(1000 + (random() % 1500)))
        .then([this](const std::string& v) { 
            trace(1);
            EXPECT_EQ(v, std::string("hello"));
            return std::string("goodbye");
        })
        )
    .then([this](const std::tuple<int, int, std::string>& t) {
        trace(2);
        EXPECT_EQ(std::get<0>(t), 10);
        EXPECT_EQ(std::get<1>(t), 20);
        EXPECT_EQ(std::get<2>(t), std::string("goodbye"));
    })
    ;
    join();
}

TEST_F(PromiseTest, test6) {
    EXPECT_CALL(*this, trace(1)).Times(3);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto p1 = delay(1, std::chrono::milliseconds(1000 + (random() % 1500)));
    auto p2 = delay(2, std::chrono::milliseconds(1000 + (random() % 1500)));
    auto p3 = delay(std::string("three"), std::chrono::milliseconds(1000 + (random() % 1500)));

    any(
        p1
        .then([this](int v) { 
            trace(1);
            EXPECT_EQ(v, 1);
            return v * 20;
        }),

        p2
        .then([this](int v) {
            trace(1);
            EXPECT_EQ(v, 2);
            return v * 20;
        }),

        p3
        .then([this](const std::string& v) { 
            trace(1);
            EXPECT_EQ(v, std::string("three"));
            return std::string("fourty two");
        })
    )
    .then([this](const tuple<int, tuple<appatis::optional_type<int>, 
                                    appatis::optional_type<int>, 
                                    appatis::optional_type<std::string>>>& t) {

        trace(2);
        auto index = get<0>(t);
        const auto& results = get<1>(t);
        switch(get<0>(t)) {
            case 0:
                EXPECT_EQ(*get<0>(results), 20);
                break;
            case 1:
                EXPECT_EQ(*get<1>(results), 40);
                break;
            case 2:
                EXPECT_EQ(*get<2>(results), std::string("fourty two"));
                break;
        }
    })
    ;
    join();
}


TEST_F(PromiseTest, test7) {
    Promise<int> pi;
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto ppc = pi.then([this](int value) {
        trace(1);
        EXPECT_EQ(value, 42);
        return Promise<const char *>();
    });

    ppc.then([this](const char *txt) {
        trace(2);
        EXPECT_STREQ(txt, "Hello!");
    });

    ppc.resolve("Hello!");
    pi.resolve(42);
    join();
}

TEST_F(PromiseTest, test7_1) {
    Promise<int> pi;
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto ppc = pi.then([this](int value) {
        trace(1);
        EXPECT_EQ(value, 42);
        return Promise<void>();
    });

    ppc.then([this] {
        trace(2);
    });

    ppc.resolve();
    pi.resolve(42);
    join();
}


TEST_F(PromiseTest, test8) {
    Promise<int> pi;
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto ppc = pi.then([this](int value) {
        trace(1);
        EXPECT_EQ(value, 42);
        return Promise<const char *>();
    });

    ppc.then([](const char *txt) {
        FAIL() << "Shouldn't get here, as the `ppc` promise not yet resolved.";
    });

    pi.resolve(42);

    pi.then([this](int v) {
        trace(2);
        EXPECT_EQ(v, 42);
    });

    pi.resolve(43); // no effect, because a promise can be fired only once.
}

TEST_F(PromiseTest, test_terminate) {
    EXPECT_DEATH({
        Promise<void> p;
        p.reject_with(0);
    }, "terminate called after throwing an instance of 'int'");
    // Promise<void> destructor called "terminate" because of unhandled rejected promise,
}


TEST_F(PromiseTest, test_unhandled_handler) {
    EXPECT_CALL(*this, trace(1)).Times(1);
    auto previous = appatis::set_unhandled_hook(test_hook);
    EXPECT_FALSE(bool(previous));
    EXPECT_FALSE(hooked);
    {
    Promise<void> p;
    p.reject_with(0); 
    }
    // Nothing happened in rejected Promise<void> destructor because of `nohandler` handler.
    trace(1);
    EXPECT_TRUE(hooked);
}

TEST_F(PromiseTest, test_ignored_reject) {
    EXPECT_CALL(*this, trace(1)).Times(1);
    {
    Promise<void> p;
    p.reject_with(0); 
    p.otherwise_continue();
    }
    // Nothing happened in rejected Promise<void> destructor because of `otherwise_continue` handler.
    trace(1);
}

TEST_F(PromiseTest, test_handled_reject) {
    EXPECT_CALL(*this, trace(1)).Times(1);
    {
    Promise<void> p;
    p.reject_with(0); 
    p.otherwise([](const std::exception_ptr&) {});
    }
    // Nothing happened in rejected Promise<void> destructor because of `otherwise` handler.
    trace(1);
}



TEST_F(PromiseTest, test10) {
    Promise<void> p;
    EXPECT_CALL(*this, trace(1)).Times(1);
    p.reject_with(0);

    p.then([this] {
        FAIL() << "Shouldn't get here.";
    })
    .otherwise([this](std::exception_ptr ep) {
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
        try {
            std::rethrow_exception(ep);
        }
        catch(int i) {
            EXPECT_EQ(i, 0);
            trace(2);
        }
        catch(...) {
            FAIL() << "Shouldn't get here.";
        }
    })
    ;
}

TEST_F(PromiseTest, test11) {
    Promise<void> p;

    p.then([this] {
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
        return 3.1415;
    })
    .then([this](int pi) {
        EXPECT_EQ(pi, 3);
        EXPECT_CALL(*this, trace(3)).Times(1);
        trace(2);
    })
    .then([this] {
        trace(3);
    })
    ;

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_TRUE(p.resolve()); // only the very first of `resolve` or `reject` succeedes.

    EXPECT_CALL(*this, trace(4)).Times(1);
    p.then([this] {
        trace(4);
    });

    p.otherwise([this](std::exception_ptr) {
        FAIL() << "Won't get here.";
    });
    EXPECT_FALSE(p.reject_with(0)); // only the very first of `resolve` or `reject` succeedes.

}


TEST_F(PromiseTest, test_promise_promise) {
    Promise<int> pi;

    pi.then([this](int v) {
        trace(1);
        EXPECT_EQ(v, 42);
        EXPECT_CALL(*this, trace(2)).Times(1);
        Promise<string> ps;
        ps.resolve("123456");
        return ps;
    })
    .then([this](const string& s) {
        EXPECT_EQ(s, "123456");
        trace(2);
    })
    ;

    EXPECT_CALL(*this, trace(1)).Times(1);
    pi.resolve(42);


}

TEST_F(PromiseTest, test_promise_post) {

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto now = std::chrono::steady_clock::now();

    auto delay = std::chrono::milliseconds(500);
    ThreadExecutor runner(delay);
    auto promise = post(runner, [&, this, now, delay] {
        trace(1);
        EXPECT_GE(std::chrono::steady_clock::now() - now, delay);
        EXPECT_TRUE(runner.in_thread());
        return 15;
    });

    promise.then([this](int value) {
        EXPECT_EQ(value, 15);
        trace(2);
    })
    .otherwise([](std::exception_ptr) {
        FAIL() << "Shouldn't get here.";
    })
    ;

}

TEST_F(PromiseTest, test_promise_post_2) {

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    auto now = std::chrono::steady_clock::now();

    auto delay = std::chrono::milliseconds(500);
    ThreadExecutor runner(delay);
    auto promise = post(runner, [&, this, now, delay] {
        trace(1);
        EXPECT_GE(std::chrono::steady_clock::now() - now, delay);
        EXPECT_TRUE(runner.in_thread());
        throw std::runtime_error("Test error");
    });

    promise.then([] {
        FAIL() << "Shouldn't get here.";
    })
    .otherwise([this](std::exception_ptr ep) {
        trace(2);
        try {
            std::rethrow_exception(ep);
        }
        catch(std::runtime_error& err) {
            EXPECT_STREQ(err.what(), "Test error");
        }
    })
    ;

}

TEST_F(PromiseTest, test_simple_int_1) {
    Promise<int> pi;
    pi.then([this](int v) {
        EXPECT_EQ(v, 42);
        EXPECT_TRUE(stage(1));
    })
    .otherwise([](std::exception_ptr) {
        FAIL() << "Cannot get here.";
    })
    ;
    EXPECT_TRUE(stage(0));
    pi.resolve(42);
    EXPECT_TRUE(stage(2));
}

TEST_F(PromiseTest, test_simple_void_1) {
    Promise<void> pi;
    pi.then([this] {
        EXPECT_TRUE(stage(1));
    })
    .otherwise([](std::exception_ptr) {
        FAIL() << "Cannot get here.";
    })
    ;
    EXPECT_TRUE(stage(0));
    pi.resolve();
    EXPECT_TRUE(stage(2));
}

TEST_F(PromiseTest, test_simple_int_2) {
    Promise<int> pi;
    pi.then([this](int v) {
        EXPECT_EQ(v, 42);
        EXPECT_TRUE(stage(1));
        throw MockException(0xdeadbeef);
    })
    .otherwise([this](std::exception_ptr ep) {
        EXPECT_TRUE(stage(2));
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            FAIL() << "Cannot get here.";
        }
    })
    ;
    EXPECT_TRUE(stage(0));
    pi.resolve(42);
    EXPECT_TRUE(stage(3));
}

TEST_F(PromiseTest, test_simple_void_2) {
    Promise<void> pi;
    pi.then([this] {
        EXPECT_TRUE(stage(1));
        throw MockException(0xdeadbeef);
    })
    .otherwise([this](std::exception_ptr ep) {
        EXPECT_TRUE(stage(2));
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            FAIL() << "Cannot get here.";
        }
    })
    ;
    EXPECT_TRUE(stage(0));
    pi.resolve();
    EXPECT_TRUE(stage(3));
}

TEST_F(PromiseTest, test_executor_1) {

    auto trivial_executor = [&](auto&& f) {
        trace(1);
        return f();
    };

    EXPECT_CALL(*this, trace(1)).Times(2);

    Promise<int> p;
    p.then(trivial_executor, [this](int v) {
        EXPECT_EQ(v, 42);
        trace(2);
    });

    p.otherwise([&](std::exception_ptr) {
        FAIL() << "Shouldn't get here.";
    });

    p.otherwise(trivial_executor, [&](std::exception_ptr) {
        FAIL() << "Shouldn't get here.";
    });


    EXPECT_CALL(*this, trace(2)).Times(1);
    p.resolve(42);

    EXPECT_CALL(*this, trace(3)).Times(1);
    p.then(trivial_executor, [this](int v) {
        EXPECT_EQ(v, 42);
        trace(3);
    });
    
}


TEST_F(PromiseTest, test_executor_2) {

    auto throwing_executor = [&](auto&& f) {
        trace(1);
        throw MockException(0xdeadbeef);
        FAIL() << "Shouldn't get here.";
    };

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);
    EXPECT_CALL(*this, trace(3)).Times(1);

    Promise<int> p;
    p.then(throwing_executor, [this](int v) {
        FAIL() << "Shouln't get here, because the executor throws.";
    })
    .otherwise([&](const std::exception_ptr& ep) {
        trace(2);
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            FAIL() << "Cannot get here.";
        }
        trace(3);
    });

    p.otherwise(throwing_executor, [&](std::exception_ptr) {
        FAIL() << "Shouln't get here, because the executor throws.";
    });

    p.resolve(42);

}

TEST_F(PromiseTest, test_executor_3) {

    auto throwing_executor = [&](auto&& f) {
        trace(1);
        throw MockException(0xdeadbeef);
        FAIL() << "Shouldn't get here.";
    };

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);
    EXPECT_CALL(*this, trace(3)).Times(1);

    Promise<int> p;

    p.otherwise(throwing_executor, [this](const std::exception_ptr&) {
        FAIL() << "Shouln't get here, because the executor throws.";
    })
    .otherwise([&](const std::exception_ptr& ep) {
        trace(2);
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            FAIL() << "Cannot get here.";
        }
        trace(3);
    });


    p.reject_with(42);

}


TEST_F(PromiseTest, test_movable_handler_1) {
    Promise<int> pi;

    int value{0};
    int dummy{0};
    auto handler = [&value, dummy = std::make_unique<int>(15)](int result) {
        value = result;
    };

    pi.then(std::move(handler));
    EXPECT_EQ(value, 0);
    pi.resolve(42);
    EXPECT_EQ(value, 42);

}


TEST_F(PromiseTest, test_movable_handler_2) {
    Promise<int> pi;

    int value{0};
    int dummy{0};
    auto handler = [&value, dummy = std::make_unique<int>(15)](int result) {
        value = result;
    };

    EXPECT_CALL(*this, trace(1)).Times(1);

    auto trivial_executor = [&](auto&& f) {
        trace(1);
        return f();
    };

    pi.then(trivial_executor, std::move(handler));
    EXPECT_EQ(value, 0);
    pi.resolve(42);
    EXPECT_EQ(value, 42);

}


        

#include <mutex>
#include <condition_variable>
#include <queue>

namespace {

    class ThreadingExecutor {
        std::thread worker_;
        std::mutex m_;
        std::condition_variable c_;
        std::atomic<bool> stop_{false};
        std::queue<std::function<void()> > tasks_;

        auto pop_task() {
        }

    public:
        ThreadingExecutor() {
            worker_ = std::thread([this] {
                while(!stop_) {
                    std::function<void()> task;
                    {
                    std::unique_lock<std::mutex> l(m_);
                    while ( !stop_ && tasks_.empty() ) {
                        c_.wait(l);
                    }
                    if ( stop_ ) {
                        return;
                    }
                    task = std::move(tasks_.front());
                    tasks_.pop();
                    }
                    task();
                }
            });
        }

        ~ThreadingExecutor() {
            worker_.join();
        }

        void stop() {
            stop_ = true;
            c_.notify_one();
        }

        template<typename F> void operator()(F&& f) {
            std::lock_guard<std::mutex> l(m_);
            tasks_.push(std::forward<F>(f));
            c_.notify_one();
        }

        bool in_thread() const {
            return std::this_thread::get_id() == worker_.get_id();
        }

    };

    template<typename EXECUTOR, typename F> auto wrap(EXECUTOR&& ex, F&& f) {
        return [ex1 = std::forward<EXECUTOR>(ex), f1 = std::forward<F>(f)] {
            return ex1(f1);
        };
    }

}


TEST_F(PromiseTest, test_executor_4) {
    EXPECT_CALL(*this, trace(0)).Times(1);
    {
    ThreadingExecutor ex;

    Promise<int> p;
    p.then([&, this](int value) {
        EXPECT_EQ(value, 42);
        EXPECT_TRUE(!ex.in_thread());
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    .then(std::ref(ex), [&, this] {
        EXPECT_TRUE(ex.in_thread());
        trace(2);
        EXPECT_CALL(*this, trace(3)).Times(1);
    })
    .then([&, this] {
        EXPECT_TRUE(ex.in_thread());
        ex.stop();
        trace(3);
        EXPECT_CALL(*this, trace(4)).Times(1);
    })
    ;

    trace(0);
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_TRUE(p.resolve(42));
    }
    trace(4);
}

TEST_F(PromiseTest, test_executor_5) {
    EXPECT_CALL(*this, trace(0)).Times(1);
    {
    ThreadingExecutor ex;

    Promise<int> p;
    p.then([&, this](int value) {
        EXPECT_EQ(value, 42);
        EXPECT_TRUE(!ex.in_thread());
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    .then(std::ref(ex), [&, this] {
        EXPECT_TRUE(ex.in_thread());
        trace(2);
        EXPECT_CALL(*this, trace(3)).Times(1);
        return 0xdeadbeef;
    })
    .then(std::ref(ex), [&, this](int v) {
        EXPECT_TRUE(ex.in_thread());
        trace(3);
        EXPECT_EQ(v, 0xdeadbeef);
        EXPECT_CALL(*this, trace(4)).Times(1);
    })
    .then([&, this] {
        ex.stop();
        trace(4);
        EXPECT_CALL(*this, trace(5)).Times(1);
    })
    ;

    trace(0);
    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_TRUE(p.resolve(42));
    }
    trace(5);
}


TEST_F(PromiseTest, test_executor_6) {
    EXPECT_CALL(*this, trace(0)).Times(1);
    {
    ThreadingExecutor ex;

    Promise<int> p;
    p.then([&, this](int value) {
        EXPECT_EQ(value, 42);
        EXPECT_TRUE(!ex.in_thread());
        trace(1);
        EXPECT_CALL(*this, trace(2)).Times(1);
    })
    .then(std::ref(ex), [&, this] {
        EXPECT_TRUE(ex.in_thread());
        trace(2);
        EXPECT_CALL(*this, trace(3)).Times(1);
        throw MockException(0xdeadbeef);
        return 17;
    })
    .then([](int v) { // could be 17, if only exception wasn't raised
        FAIL() << "Shouldn't get here.";
    })
    .otherwise(std::ref(ex), [&, this](std::exception_ptr ep) {
        EXPECT_TRUE(ex.in_thread());
        trace(3);
        try {
            std::rethrow_exception(ep);
        }
        catch(MockException& me) {
            EXPECT_EQ(me.value_, 0xdeadbeef);
        }
        catch(...) {
            ADD_FAILURE() << "Shouldn't get here.";
        }
        EXPECT_CALL(*this, trace(4)).Times(1);
        return 100500;
    })
    .then(std::ref(ex), [&, this](int v) {
        EXPECT_EQ(v, 100500);
        EXPECT_TRUE(ex.in_thread());
        ex.stop();
        trace(4);
        EXPECT_CALL(*this, trace(5)).Times(1);
    })
    ;

    trace(0);
    EXPECT_CALL(*this, trace(1)).Times(1);
    p.resolve(42);
    }
    trace(5);
}

TEST_F(PromiseTest, get_value_1) {
    Promise<int> p;

    p.resolve(5);

    EXPECT_EQ(wait_for_value(p), 5);
}

TEST_F(PromiseTest, get_value_2) {

    EXPECT_CALL(*this, trace(1)).Times(1);
    EXPECT_CALL(*this, trace(2)).Times(1);

    Promise<int> p;
    p.reject_with(MockException(777));

    try {
        wait_for_value(p);
        FAIL() << "Shouldn't get here.";
    }
    catch ( MockException& me ) {
        trace(1);
        EXPECT_EQ(me.value_, 777);
    }
    catch ( ... ) {
        FAIL() << "Shouldn't get here.";
    }
    trace(2);
    
    p.otherwise_continue();
}

TEST_F(PromiseTest, all_done_1) {

    const int nthreads{500};

    std::vector<int> vb(nthreads);

    bool latch{false};
    std::condition_variable cond;
    std::mutex lm;

    std::vector<std::thread> herd;
    herd.reserve(nthreads);

    for ( const auto& i: vb ) {
        EXPECT_EQ(i, 0);
    }

    Promise<void> p;
    p.resolve();

    for ( int i = 0; i < nthreads; ++i ) {
        herd.emplace_back([&, i] {
            do {
                std::unique_lock<std::mutex> l(lm);
                while ( !latch ) {
                    cond.wait(l);
                }
            } while(0);
            p.then([&, i] { ++vb[i]; });
        });
    }

    {
        std::lock_guard<std::mutex> l(lm);
        latch = true;
        cond.notify_all();
    }

    for ( auto& thread: herd ) {
        thread.join();
    }
    
    for ( const auto& i: vb ) {
        EXPECT_EQ(i, 1);
    }
}


TEST_F(PromiseTest, all_done_2) {

    const int nthreads{500};

    std::vector<std::atomic<int>> vb(nthreads);

    std::atomic<int> result{-1};

    bool latch{false};
    std::condition_variable cond;
    std::mutex lm;

    std::vector<std::thread> herd;
    herd.reserve(nthreads);

    for ( auto& rvb: vb ) {
        rvb.store(0);
    }

    Promise<int> p;

    for ( int i = 0; i < nthreads; ++i ) {
        herd.emplace_back([&, i] {
            do {
                std::unique_lock<std::mutex> l(lm);
                while ( !latch ) {
                    cond.wait(l);
                }
            } while(0);
            p.then([&, i](int) { 
                auto prev = vb[i].fetch_add(1); 
                EXPECT_EQ(prev, 0);
            });
            if ( i % 2 == 0 ) {
                if ( p.resolve(i) ) {
                    EXPECT_EQ(result.exchange(i), -1);
                }
            }
        });
    }

    {
        std::lock_guard<std::mutex> l(lm);
        latch = true;
        cond.notify_all();
    }

    for ( auto& thread: herd ) {
        thread.join();
    }
    herd.clear();

    EXPECT_TRUE(p.done());
    EXPECT_CALL(*this, trace(0)).Times(1);
    p.then([&](int i) {
        EXPECT_EQ(i, result.load());
        trace(0);
    });

    
    for ( auto& i: vb ) {
        EXPECT_EQ(i.load(), 1);
        i.store(0);
    }

    for ( int i = 0; i < nthreads; ++i ) {
        herd.emplace_back([&, i] {
            do {
                std::unique_lock<std::mutex> l(lm);
                while ( !latch ) {
                    cond.wait(l);
                }
            } while(0);
            EXPECT_EQ(p.resolve(i), false);
        });
    }

    {
        std::lock_guard<std::mutex> l(lm);
        latch = true;
        cond.notify_all();
    }

    for ( auto& thread: herd ) {
        thread.join();
    }


    for ( const auto& i: vb ) {
        EXPECT_EQ(i.load(), 0);
    }
}



TEST_F(PromiseTest, concurrent_resolve_reject_1) {

    const int nthreads{500};

    struct Value {
        int toolarge[10];
        Value(int i) {
            for ( unsigned count = 0; count < sizeof(toolarge)/sizeof(toolarge[0]); toolarge[count++] = i );
        }
        bool all(int i) const {
            return std::all_of(&toolarge[0], toolarge+sizeof(toolarge)/sizeof(toolarge[0]), [i](const int& rv) { return rv == i; });
        }
    };

    enum Values { NONE = 0, RESOLVED, REJECTED, BEFORE, AFTER };

    struct result {
        Value value{-1};
        std::atomic<int> op{NONE};
        std::atomic<int> when{NONE};
    };
    std::atomic<int> succeeded_number{-1};
    std::atomic<int> succeeded_how{NONE};
    
    std::vector<result> vb(nthreads);

    for ( const auto& rr: vb ) {
        EXPECT_TRUE(rr.value.all(-1) && rr.op == NONE && rr.when == NONE);
    }

    bool latch{false};
    std::condition_variable cond;
    std::mutex lm;

    std::vector<std::thread> herd;
    herd.reserve(nthreads);

    Promise<Value> p;

    for ( int i = 0; i < nthreads; ++i ) {
        herd.emplace_back([&, i] {
            do {
                std::unique_lock<std::mutex> l(lm);
                while ( !latch ) {
                    cond.wait(l);
                }
            } while(0);
            p.then([&, i] (const Value& v) {
                EXPECT_TRUE(vb[i].value.all(-1));
                vb[i].value = v; 
            })
            .otherwise_continue(); // without .otherwise_continue or .otherwise handler, rejected promise will terminate the program like unhandled exception.

            p.otherwise([&, i](const std::exception_ptr& ep) { 
                EXPECT_TRUE(vb[i].value.all(-1));
                try {
                    std::rethrow_exception(ep);
                }
                catch ( int& i1000 ) {
                    vb[i].value = i1000;
                }
            });

            int op_prev{NONE};
            int when_prev{NONE};

            int succeeded_number_prev{-1};
            int succeeded_how_prev{NONE};

            // prepare an exception_ptr beforehand, to save time

            int resolve_or_reject = std::hash<long>()(time(0)) & 1;

            try {
                throw i * 1000;
            }
            catch ( int& ) {
                auto ep = std::current_exception();

                if ( resolve_or_reject ) {

                    if ( p.resolve(i) ) {
                        EXPECT_TRUE(vb[i].when.compare_exchange_strong(when_prev, BEFORE));
                        EXPECT_TRUE(succeeded_number.compare_exchange_strong(succeeded_number_prev, i));
                        EXPECT_TRUE(succeeded_how.compare_exchange_strong(succeeded_how_prev, RESOLVED));
                    }
                    else {
                        EXPECT_TRUE(vb[i].when.compare_exchange_strong(when_prev, AFTER));
                    }
                    EXPECT_TRUE(vb[i].op.compare_exchange_strong(op_prev, RESOLVED));
                }
                else {
                    if ( p.reject(std::move(ep)) ) {
                        EXPECT_TRUE(vb[i].when.compare_exchange_strong(when_prev, BEFORE));
                        EXPECT_TRUE(succeeded_number.compare_exchange_strong(succeeded_number_prev, i));
                        EXPECT_TRUE(succeeded_how.compare_exchange_strong(succeeded_how_prev, REJECTED));
                    }
                    else {
                        EXPECT_TRUE(vb[i].when.compare_exchange_strong(when_prev, AFTER));
                    }
                    EXPECT_TRUE(vb[i].op.compare_exchange_strong(op_prev, REJECTED));
                }
            }

        });
    }

    {
        std::lock_guard<std::mutex> l(lm);
        latch = true;
        cond.notify_all();
    }

    for ( auto& thread: herd ) {
        thread.join();
    }
    
    EXPECT_TRUE(succeeded_number >= 0 && succeeded_number < vb.size());
    EXPECT_TRUE(succeeded_how == RESOLVED || succeeded_how == REJECTED);

    EXPECT_EQ(vb[succeeded_number].op, succeeded_how);
    if ( succeeded_how == RESOLVED ) {
        EXPECT_TRUE(vb[succeeded_number].value.all(succeeded_number));
    }
    else {
        EXPECT_TRUE(vb[succeeded_number].value.all(succeeded_number*1000));
    }

    EXPECT_EQ(vb[succeeded_number].when, BEFORE);

    for ( unsigned index = 0; index < vb.size(); ++index ) {
        const auto& rvb = vb[index];
        EXPECT_TRUE(rvb.when == BEFORE || rvb.when == AFTER);
        EXPECT_TRUE(rvb.op == RESOLVED || rvb.op == REJECTED);
        if ( rvb.when == BEFORE ) {
            EXPECT_EQ(index, succeeded_number);
            if ( rvb.op == RESOLVED ) {
                EXPECT_TRUE(rvb.value.all(succeeded_number));
            }
            else {
                EXPECT_TRUE(rvb.value.all(succeeded_number * 1000));
            }
        }
    }
}



TEST_F(PromiseTest, large_object) {

    struct LargeObject {
        int v_;
        std::vector<char> padding_;

        explicit LargeObject(int i) : v_{i}, padding_(1024) {}
        LargeObject(const LargeObject&) {
            fail();
        }
        LargeObject(LargeObject&& other) = default;

        void fail() {        
            FAIL() << "Shouldn't call a copy constructor.";
        }
    };

    const int value = 4242;

    Promise<LargeObject> p;

    EXPECT_CALL(*this, trace(0)).Times(1);
    p.then([this, value](const LargeObject& lo) {
        EXPECT_EQ(lo.v_, value);
        trace(0);
        EXPECT_CALL(*this, trace(1)).Times(1);
    });

    p.resolve(LargeObject(value));
    trace(1);

}


TEST_F(PromiseTest, default_argument_1) {
    Promise<int> p;
    p.then([this](int v) {
        trace(0);
        EXPECT_EQ(v, 0);
        EXPECT_CALL(*this, trace(1)).Times(1);
    });
    EXPECT_CALL(*this, trace(0)).Times(1);
    p.resolve(); // constructs int value with default constructor
    trace(1);
}


TEST_F(PromiseTest, default_argument_2) {
    
    struct Inner {
        int v_;
        Inner() : v_(15) {}
    };

    Promise<Inner> p;
    p.then([this](const Inner& v) {
        trace(0);
        EXPECT_EQ(v.v_, 15);
        EXPECT_CALL(*this, trace(1)).Times(1);
    });
    EXPECT_CALL(*this, trace(0)).Times(1);
    p.resolve(); // constructs a value with default constructor
    trace(1);
}


TEST_F(PromiseTest, forward_arguments_1) {
    
    struct Inner {
        std::string msg_;
        Inner(int a, const std::string& b) {
            std::stringstream out;
            out << b << ": " << a;
            msg_ = out.str();
        }
        Inner(const Inner&) {
            fail();
        }
        void fail() const {
            FAIL() << "Copy constructor should not be called";
        }
    };

    Promise<Inner> p;
    p.then([this](const Inner& v) {
        trace(0);
        EXPECT_EQ(v.msg_, "Value: 7");
        EXPECT_CALL(*this, trace(1)).Times(1);
    });
    EXPECT_CALL(*this, trace(0)).Times(1);
    p.resolve(7, "Value"); // the promise gets an object Inner(7, "Value") constructed in-place
    trace(1);
}



int main(int argc, char **argv) {
    srandom(time(NULL));
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
