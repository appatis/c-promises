#!/usr/bin/env python3
import os, contextlib, sys
import argparse

flavors = [
    {
        'lockfree': ['PROMISE_IS_LOCKFREE', '1'],
        'nolockfree': ['PROMISE_IS_LOCKFREE', '0'],
    },
    {
        'debug': ['CMAKE_BUILD_TYPE', 'Debug'],
        'release': ['CMAKE_BUILD_TYPE', 'Release'],
    },
    {
        'none': [],
        'thread': ['SANITIZE', 'thread'],
        'leak': ['SANITIZE', 'leak'],
        'addr': ['SANITIZE', 'address'],
        'undef': ['SANITIZE', 'undefined'],
    },
    {
        "14": ['CMAKE_CXX_STANDARD', '14'],
        "17": ['CMAKE_CXX_STANDARD', '17'],
    }
]


def find_compilers():
    compilers = []

    def try_exec(name):
        for dir in ("", "/usr/bin", "/usr/local/bin", "/opt/bin"):
            fp = os.path.join(dir, name)
            if os.system(f"{fp} --version 2>/dev/null >/dev/null") == 0:
                compilers.append(fp)
                return

    try_exec("g++")
    try_exec("clang++")
    for version in (str(n) for n in range(17, 13, -1)):
        try_exec(f"clang++-{version}")

    return compilers


def find_srcdir():
    path_up = ["."]
    while not os.path.isfile(os.path.join(*path_up, "CMakeLists.txt")):
        path_up.append("..")
    return os.path.join(*path_up)


@contextlib.contextmanager
def mkchdir(dirname):
    try:
        os.mkdir(dirname)
    except:
        pass
    prev = os.getcwd()
    os.chdir(dirname)
    yield
    os.chdir(prev)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--directory", help="build top directory", default="build")
    parser.add_argument("-c", "--compilers", help="compilers list to build with", nargs="+")
    args = parser.parse_args()
    if not args.compilers:
        args.compilers = find_compilers()

    with mkchdir("build"):
        builds = []
        for compiler in args.compilers:
            compiler_basename = os.path.split(compiler)[-1]
            for lockfree in flavors[0].items():
                for build in flavors[1].items():
                    for san in flavors[2].items():
                        for std in flavors[3].items():
                            dirname=f"{compiler_basename}-c++{std[0]}-{lockfree[0]}-{build[0]}-{san[0]}"
                            print(dirname)
                            with mkchdir(dirname):
                                # command = f"cmake -D {compiler[1][0]}={compiler[1][1]} -D {lockfree[1][0]}={lockfree[1][1]} -D {build[1][0]}={build[1][1]} -D {std[1][0]}={std[1][1]}"
                                command = f"cmake -D CMAKE_CXX_COMPILER={compiler} -D {lockfree[1][0]}={lockfree[1][1]} -D {build[1][0]}={build[1][1]} -D {std[1][0]}={std[1][1]}"
                                if san[1]:
                                    command += f" -D {san[1][0]}={san[1][1]}"
                                srcdir = find_srcdir()
                                command += f" {srcdir}"
                                with open("cmake.cmd", "w") as fd:
                                    fd.write(command)
                                print(f"---------------------------\nIn {dirname}:\n  {command}\n\n")
                                assert os.system(command) == 0, command
                            builds.append(dirname)

        for build in builds:
            with mkchdir(build):
                print(f"\n=========================== {build}\n");
                with open('cmake.cmd') as fd:
                    command = fd.read()
                assert os.system("make") == 0, command
                test_cmd = "./test --gtest_break_on_failure"
                assert os.system(test_cmd) == 0, command


if __name__ == "__main__":
    main()
